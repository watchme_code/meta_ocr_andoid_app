package net.reqres.metaocr

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.canhub.cropper.CropImage.ActivityResult
import com.canhub.cropper.CropImageActivity
import com.canhub.cropper.CropImageView
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import net.reqres.metaocr.databinding.ExtendedActivityBinding

internal class SampleCustomActivity : CropImageActivity() {

  companion object {
    const val TAG = "SampleCustomActivity"
    fun start(activity: Activity) {
      ActivityCompat.startActivity(
        activity,
        Intent(activity, SampleCustomActivity::class.java),
        null,
      )
    }
  }

  private lateinit var binding: ExtendedActivityBinding
  private var recognizer: TextRecognizer? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    binding = ExtendedActivityBinding.inflate(layoutInflater)

    super.onCreate(savedInstanceState)

    binding.saveBtn.setOnClickListener { cropImage() }
    binding.backBtn.setOnClickListener { onBackPressedDispatcher.onBackPressed() }
    binding.rotateText.setOnClickListener { onRotateClick() }

    binding.cropImageView.setOnCropWindowChangedListener {
      updateExpectedImageSize()
    }

    recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    setCropImageView(binding.cropImageView)
  }

  override fun onSetImageUriComplete(
    view: CropImageView,
    uri: Uri,
    error: Exception?,
  ) {
    super.onSetImageUriComplete(view, uri, error)

    updateRotationCounter()
    updateExpectedImageSize()
  }

  private fun updateExpectedImageSize() {
    binding.expectedImageSize.text = binding.cropImageView.expectedImageSize().toString()
  }

  override fun setContentView(view: View) {
    super.setContentView(binding.root)
  }

  private fun updateRotationCounter() {
    binding.rotateText.text = "Rotate" + binding.cropImageView.rotatedDegrees.toString()
  }

  override fun onPickImageResult(resultUri: Uri?) {
    super.onPickImageResult(resultUri)

    if (resultUri != null) {
      binding.cropImageView.setImageUriAsync(resultUri)
    }
  }

  override fun getResultIntent(uri: Uri?, error: java.lang.Exception?, sampleSize: Int): Intent {
    val result = super.getResultIntent(uri, error, sampleSize)
    // Adding some more information.
    return result.putExtra("EXTRA_KEY", "Extra data")
  }

  override fun setResult(uri: Uri?, error: Exception?, sampleSize: Int) {
    val res = ActivityResult(
      originalUri = binding.cropImageView.imageUri,
      uriContent = uri,
      error = error,
      cropPoints = binding.cropImageView.cropPoints,
      cropRect = binding.cropImageView.cropRect,
      rotation = binding.cropImageView.rotatedDegrees,
      wholeImageRect = binding.cropImageView.wholeImageRect,
      sampleSize = sampleSize,
    )

    val image: InputImage
    try {
      image = InputImage.fromFilePath(this, uri!!)
      val r = recognizer?.process(image)!!
        .addOnSuccessListener { result ->
          // Task completed successfully
          val resultText = result.text
          for (block in result.textBlocks) {
            val blockText = block.text
            val blockCornerPoints = block.cornerPoints
            val blockFrame = block.boundingBox
            for (line in block.lines) {
              val lineText = line.text
              Log.i(TAG, lineText)
              val lineCornerPoints = line.cornerPoints
              val lineFrame = line.boundingBox
              for (element in line.elements) {
                val elementText = element.text
                val elementCornerPoints = element.cornerPoints
                val elementFrame = element.boundingBox
              }
            }
          }

        }
        .addOnFailureListener { e ->
          // Task failed with an exception
          Toast.makeText(this, "Fail -OCR:" + e.message, Toast.LENGTH_LONG).show()
        }
    } catch (e: Exception) {
      e.printStackTrace()
    }



//    Timber.tag("AIC-Sample").i("Original bitmap: ${result.originalBitmap}")
//    Timber.tag("AIC-Sample").i("Original uri: ${result.originalUri}")
//    Timber.tag("AIC-Sample").i("Output bitmap: ${result.bitmap}")
//    Timber.tag("AIC-Sample").i("Output uri: ${result.getUriFilePath(this)}")
    binding.cropImageView.setImageUriAsync(res.uriContent)
  }

  override fun setResultCancel() {
//    Timber.tag("AIC-Sample").i("User this override to change behaviour when cancel")
    super.setResultCancel()
  }

  override fun updateMenuItemIconColor(menu: Menu, itemId: Int, color: Int) {
//    Timber.tag("AIC-Sample").i("If not using your layout, this can be one option to change colours")
    super.updateMenuItemIconColor(menu, itemId, color)
  }

  private fun onRotateClick() {
    binding.cropImageView.rotateImage(90)
    updateRotationCounter()
  }


}
