# MetaOCR Android App

## Features

MetaOCR is equipped with a suite of features aimed at simplifying the process of text recognition from images:

- **Text Recognition**: Utilize Google's OCR technology to accurately extract text from images.
- **Save Functionality**: Easily save recognized text for future reference.
- **Image Cropping**: Select and crop specific parts of an image to recognize text from.
- **Custom UI**: Enjoy a beautifully designed user interface with custom assets created by Adobe Illustrator.
- **User-friendly Experience**: Designed with simplicity and ease of use in mind, suitable for all users.

## Getting Started

To get started with MetaOCR, follow these steps:

### Prerequisites

- Android Studio
- Kotlin Plugin for Android Studio
- An Android device or emulator with Android version 5.0 (Lollipop) or higher

## Technologies Used

- **Kotlin**: For developing the app, taking advantage of its concise syntax and safety features.
- **Google OCR ML Library**: For text recognition capabilities, ensuring accurate and efficient text extraction.
- **Adobe Illustrator**: For creating custom assets, providing a unique and engaging user interface.